/* 
Wait for DOM content
*/
document.addEventListener('DOMContentLoaded', () => {
    /* 
    # Mise en place d'un lecteur de support de cours
    L'exercice qui vous est proposé vas vous permettre de réaliser une WebApp qui affiche les supports de cours qui vous on été fourni lors de vos sessions.
    Les principes à mettre en place sont exactement les mêmes que ceux vu en cours, aucun piège dans cet exercice.
    Il vous suffit d'être capable de répéter ce que vous avez vu en cours dans un cadre légèrement différent.
    Et en vous proposant de découvrir plus en profondeur le format Markdown. 
    
    Ce format est celui utilisé pour vos supports de cours, mais aussi pour les fichiers README que vous devez créer pour chacun de vos répertoires.
    Le principe de ce format est qu'il est facile a convertir dans d'autres formats, par exemple en HTML.
    Ce que vous allez devoir faire dans cet exercice et d'éxécuter une requête vers des fichiers Markdown, pour les afficher en HTML dans votre page.
    Pour ce faire, vous avez pour commencer toutes les techniques vues en cours.
    Puis le module Marked (https://www.npmjs.com/package/marked), qui est déja chargé dans le fichier 'index.html'.
    Affichez la page web dans votre navigateur et inspectez votre console, l'intéret du format Markdonw est la simplicité avec laquelle on peut le convertir.

    Prennez le temps de bien lire l'ennoncé suivant, et bon courage!

    ## Liste des tâches :
    - Capter l'événement clic sur chaque bouton de la section 'section-courses-list'
    - Une fois l'événement capté, récupérer la valeur de la propriété 'data-link'
    - Utiliser la valeur de 'data-link' pour exécuter une requête HTTP afin de récupérer le contenu d'un support de cours
    - Une fois le contenu récupéré, créer une section dans le DOM pour l'afficher avec Marked
    */
	let debug = undefined;
    let link = undefined;
    const section = document.querySelectorAll("#section-courses-list button");
    section.forEach(element =>
    element.addEventListener('click', event => {
        if((link = element.dataset.link) != undefined)
        console.log(link);
        getTextCollection(link);
    })
    );
 
    function getTextCollection(link){ 
        const fetchRequest = new FetchRequest(
            `${link}`,
            `text`
        )
        fetchRequest.init()

        // Send request and use Promise
        fetchRequest.sendRequest()
        .then( fetchResponse => {
            let newSection = document.createElement("section");
            let text = document.createTextNode(marked.parse(fetchResponse));
            newSection.appendChild(text);
            let section = document.getElementById("section-courses-list");
            section.appendChild(newSection);
            section.innerHTML = marked.parse(fetchResponse);
        })
        .catch( fetchError => {
            // Debug
            if(debug){
                console.log('Fetch error', fetchError)
            }
        })
    }
})